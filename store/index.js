export const getters = {
  viewport: (state) => state.viewport.viewport,
  navActive: (state) => state.Navigation.active,
  config: (state) => state.config.config
}

export const strict = false
