export default ({ app }) => {
  app.router.beforeEach((to, from, next) => {
    window.scrollTo(0, 0)
    next()
  })
}
