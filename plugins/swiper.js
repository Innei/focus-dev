import Vue from 'vue'
import { swiper, swiperSlide } from 'vue-awesome-swiper'

Vue.component('swiper', swiper)
Vue.component('swiperSlide', swiperSlide)
